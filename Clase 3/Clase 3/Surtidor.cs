﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Clase_3
{
    public class Surtidor
    {
        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }



        private float recaudacion;

        private int capacidad;

        public int Capacidad
        {
            get { return capacidad; }
            set { capacidad = value; }
        }

        private float nivel;

        public float Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }



        private Nafta nafta;
        public Nafta Nafta
        {
            get { return nafta; }
            set
            {
                nafta = value;
            }
        }

        private int cantidadVentas;

        public int CantidadVentas
        {
            get { return cantidadVentas; }
         
        }

        private int cantidadRecargas;

        public int CantidadRecargas
        {
            get { return cantidadRecargas; }
   
        }


        public bool Vender(float litros)
        {
            bool ok = (litros <= this.nivel);

            if(ok)
            {
                recaudacion += this.nafta.Vender(litros) ;
                cantidadVentas++;
                nivel -= litros;
            }

            return ok;
        }

        public void Recargar()
        {
            this.cantidadRecargas++;
            this.nivel = this.capacidad;
        }

        public float ObtenerRecaudación()
        {
            return recaudacion;
        }

    }
}